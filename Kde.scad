module k(h)
{
    linear_extrude(height=h)
      polygon(points=[[-1.489262,0.497089],[-1.489262,4.194175],[-4.500000,4.404912],[-4.500000,-4.258428],[-1.489262,-4.500002],[-1.489262,-0.782361],[1.756633,-4.500002],[4.499999,-3.870369],[1.185959,-0.123356],[4.500000,3.888348],[1.718924,4.500002],[-1.489262,0.497089]]);
}

module d(h)
{
    difference($fn=50){
            linear_extrude(height=h){
            polygon(points=[[-1.470235,4.499999],[-4.500000,4.499999],[-4.500000,-4.499999],[-1.313962,-4.499999],[1.741318,-3.997724],[2.910967,-3.371928],[3.779234,-2.499089],[4.313430,-1.380572],[4.500000,-0.039580],[4.027994,1.849412],[2.749114,3.293445],[0.829199,4.188811],[-1.470235,4.499999],[-1.470235,4.499999]]);
        }
        
        linear_extrude(height=h){
            polygon(points=[[-1.693480,-2.488170],[-1.693480,2.398090],[-0.092488,2.227480],[0.972714,1.715651],[1.570694,0.930165],[1.770021,-0.061418],[1.770021,-0.129658],[1.517273,-1.261142],[0.841956,-1.974972],[-0.243179,-2.348949],[-1.693480,-2.488166],[-1.693480,-2.488170]]);
  scale([25.4/90, -25.4/90, 1]);
        }

    }
}

module e(h)
{
    linear_extrude(height=h)
      polygon(points=[[-4.500000,4.499999],[-4.500000,-4.499999],[3.818058,-4.499999],[4.497915,-2.510010],[-1.068346,-2.510010],[-1.068346,-1.090537],[2.674910,-1.090537],[2.674910,0.899451],[-1.100720,0.899451],[-1.100720,2.510010],[4.500000,2.510010],[3.621852,4.499999],[-4.500000,4.499999]]);
}

intersection (){
    intersection (){
        #k(9);

        translate([-4.5,0,4.5]){
            rotate([90,0,90]) {
                #d(9);
            }
        }
    }

    translate([0, 4.5,4.5]){
        rotate ([90,90,0]){
            #e(9);
        }
    }
}